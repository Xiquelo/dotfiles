-- Pull in the wezterm API
local wezterm = require("wezterm")

-- This will hold the configuration
local config = wezterm.config_builder()

config.font = wezterm.font("GohuFont 11 Nerd Font")
config.font_size = 22

config.enable_tab_bar = false
config.window_decorations = "RESIZE"

config.window_background_opacity = 0.7

return config
