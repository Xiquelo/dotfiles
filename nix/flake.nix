{
  description = "Xiquelo's Darwin system flake";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    nix-darwin.url = "github:LnL7/nix-darwin";
    nix-darwin.inputs.nixpkgs.follows = "nixpkgs";
    nix-homebrew.url = "github:zhaofengli-wip/nix-homebrew";
  };

  outputs = inputs@{ self, nix-darwin, nixpkgs, nix-homebrew }:
  let
    configuration = { pkgs, config, ... }: {

      nixpkgs.config.allowUnfree = true;

      # List packages installed in system profile. To search by name, run:
      # $ nix-env -qaP | grep wget
      environment.systemPackages =
        [ 
	  pkgs.git
	  pkgs.lazygit
	  pkgs.neovim 
	  pkgs.tmux 
	  pkgs.lsd
	  pkgs.bat
	  pkgs.zoxide
	  pkgs.mkalias
	  pkgs.python3
	  pkgs.telegram-desktop
	  pkgs.raycast
	  pkgs.discord
	  pkgs.dolphin-emu
        ];

      fonts.packages = with pkgs; [
	pkgs.nerd-fonts.terminess-ttf
      ];

      homebrew = {
        enable = true;
	brews = [
	  "mas"
	  "oh-my-posh"
	];
	casks = [
	  "font-gohufont-nerd-font"
	  "setapp"
	  "wezterm"
	  "the-unarchiver"
	  "simplenote"
	  "whatsapp@beta"
	  "messenger"
	  "chatgpt"
	  "sublime-text"
	  "transmission"
	  "microsoft-teams"
	  "skype"
	  "zen-browser"
	  "alex313031-thorium"
	  "readdle-spark"
	  "visual-studio-code"
	  "rider"
	  "steam"
	  "clone-hero"
	  "gog-galaxy"
	  "crossover"
	  "amorphousdiskmark"
	];
	masApps = {
	  "Magnet" = 441258766;
	  #"Speedtest" = 1153157709;
	  # "Bitwarden" = 1352778147;
	  "Word" = 462054704;
	  "PowerPoint" = 462062816;
	  "Excel" = 462058435;
	  "Nightshift" = 1561604170;
	  "DiskSpeedTest" = 425264550;
	};
	onActivation.cleanup = "zap";
	onActivation.autoUpdate = true;
	onActivation.upgrade = true;
      };
      
      system.activationScripts.applications.text = let
      	env = pkgs.buildEnv {
	  name = "system-applications";
	  paths = config.environment.systemPackages;
	  pathsToLink = "/Applications";
	};
      in
      	pkgs.lib.mkForce ''
	# Set up applications.
	echo "setting up /Applications..." >&2
	rm -rf /Applications/Nix\ Apps
	mkdir -p /Applications/Nix\ Apps
	find ${env}/Applications -maxdepth 1 -type l -exec readlink '{}' + |
	while read -r src; do
	  app_name=$(basename "$src")
	  echo "copying $src" >&2
	  ${pkgs.mkalias}/bin/mkalias "$src" "/Applications/Nix Apps/$app_name"
	done
	    '';

      system.defaults = {
      	dock.persistent-apps = [
	  "/Applications/Safari.app"
	  "/Applications/ChatGPT.app"
	  "/Applications/Spark Desktop.app"
	  "/System/Applications/Notes.app"
	  "/System/Applications/Photos.app"
	  "/System/Applications/Music.app"
	  "/System/Applications/App Store.app"
	  "/Applications/Whatsapp.app"
	  "/Applications/Messenger.app"
	  "${pkgs.telegram-desktop}/Applications/Telegram.app"
	  "${pkgs.discord}/Applications/Discord.app"
	  "/Applications/Simplenote.app"
	  "/Applications/Skype.app"
	  "/Applications/Microsoft Teams.app"
	  "/Applications/Visual Studio Code.app"
	  "/Applications/Rider.app"
	  "/Applications/Steam.app"
	  "/Applications/GOG Galaxy.app"
	  "/Applications/WezTerm.app"
	  "/System/Applications/System Settings.app"
	];
	/*dock.persistent-others = [
	  "~/Documents"
	  "~/Downloads"
	];*/
	dock.largesize = 100;
	dock.tilesize = 45;
	dock.showhidden = true;
	dock.show-recents = false;
	dock.wvous-tr-corner = 14;
	dock.wvous-br-corner = 11;
	dock.wvous-bl-corner = 13;
	finder.AppleShowAllExtensions = true;
	finder.ShowPathbar = true;
	finder.ShowStatusBar = true;
	loginwindow.GuestEnabled = false;
	NSGlobalDomain."com.apple.swipescrolldirection" = false;
	WindowManager.StandardHideDesktopIcons = true;
      };
      
      # Necessary for using flakes on this system.
      nix.settings.experimental-features = "nix-command flakes";

      # Enable alternative shell support in nix-darwin.
      # programs.fish.enable = true;

      # Set Git commit hash for darwin-version.
      system.configurationRevision = self.rev or self.dirtyRev or null;

      # Used for backwards compatibility, please read the changelog before changing.
      # $ darwin-rebuild changelog
      system.stateVersion = 5;

      # The platform the configuration will be used on.
      nixpkgs.hostPlatform = "aarch64-darwin";
    };
  in
  {
    # Build darwin flake using:
    # $ darwin-rebuild build --flake .#simple
    darwinConfigurations."macx" = nix-darwin.lib.darwinSystem {
      modules = [ 
        configuration 
	nix-homebrew.darwinModules.nix-homebrew
	{
	  nix-homebrew = {
	    enable = true;
	    # Apple Silicon Only
	    enableRosetta = true;
	    # User owning the Homebrew prefix
	    user = "xiquelo";
	  };
	}
      ];
    };
  };
}
