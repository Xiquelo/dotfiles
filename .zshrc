# Oh-My-Posh
eval "$(oh-my-posh init zsh --config $(brew --prefix oh-my-posh)/themes/blue-owl.omp.json)"

# Zoxide
eval "$(zoxide init zsh)"

# Aliases
alias nixup='nix flake update ~/dotfiles/nix'
alias rebuild='darwin-rebuild switch --flake ~/dotfiles/nix#macx'
alias reload='source ~/.zshrc'
alias z..='z ..'
alias z...='z ../..'
alias z....='z ../../..'
alias z.....='z ../../../..'
alias ls='lsd'
alias lsa='lsd -a'
alias lsal='lsd -al'
alias v='nvim'

# git alias
alias init='git init'
alias add='git add'
alias gcm='gc -m'
alias gcam='gc -am'
alias amd='gc -ammend -m'
alias gits='git status -sb'
alias log="git log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all"
alias push='git push -u origin main'
alias pull='git pull'
alias branch='git branch'
alias branchd='git branch -d'
alias reset='git reset'
alias check='git checkout'
alias checkb='git checkout -b'
